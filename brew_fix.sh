#!/bin/zsh

## Does the job
function worker {
	rm -rf ~/.brew;
	if [[ $ZSHRC_MOD_STATUS -eq 0 ]] ; then
		zshrc_setup;
	fi;
	if [[ $BREW_MOD_STATUS -eq 0 ]] ; then
		add_brew_fix;
	fi;
	source $DEFAULT_ZSHRC;
	echo "\x1b[33mUpdating brew cache, it can take some time\x1b[0m"
	/usr/local/bin/brew update;
	echo "\x1b[32mDone!\x1b[0m";
	if [ -f ~/.brew_autoinstall ] ; then
		echo "\x1b[33m$AUTOINST_FILE found, starting autoinstall\x1b[0m";
	autoinstall;
	fi
}

## Handle autoinstall of the packages in ~/.brew_autoinstall
function autoinstall {
	for target in `cat ~/.brew_autoinstall`
		do brew install $target;
	done;
	echo "\x1b[32mAutoinstall finished\x1b[0m"
	echo "\x1b[33mUpgrading existing package\x1b[0m"
	$HOME/.brew/bin/brew upgrade
	echo "\x1b[32m ~~~ The End ~~~ \x1b[0m"
}

## Add the fix for brew in $MODDED_ZSHRC
function add_brew_fix {
	echo "## Homebrew fix UUID:b9b74f04-2c78-458f-895e-45f6db30a898" >> $MODDED_ZSHRC;
	echo "# Protector: UUID:b9b74f04-2c78-458f-895e-45f6db30a898" >> $MODDED_ZSHRC;
	echo "export PATH=$HOME/.brew/bin:$PATH" >> $MODDED_ZSHRC;
	echo "export HOMEBREW_CACHE=/tmp/$USER/Homebrew/cache" >> $MODDED_ZSHRC;
	echo "export HOMEBREW_TEMP=/tmp/$USER/Homebrew/temp" >> $MODDED_ZSHRC;
	echo "mkdir -p /tmp/$USER/Homebrew/tmp /tmp/$USER/Homebrew/cache ~/Library/Caches/Homebrew /tmp/$USER/Homebrew/Locks" >> $MODDED_ZSHRC;
	echo "rm -rf ~/.brew/Library/Locks" >> $MODDED_ZSHRC;
	echo "### End of the Homebrew fix" >> $MODDED_ZSHRC;
	echo "\x1b[32mBrew fix added\x1b[0m";
}

## Make it so zsh will load $MODDED_ZSHRC for our mods
function zshrc_setup {
	echo "## Allow to put mofication to $MODDED_ZSHRC to keep this one clean" >> $DEFAULT_ZSHRC;
	echo "# Protector: UUID=d1a5d801-dde1-47e1-8eef-83720894dfd6" >> $DEFAULT_ZSHRC;
	echo "source $MODDED_ZSHRC" >> $DEFAULT_ZSHRC;
	echo "### End of $MODDED_ZSHRC sourcing" >> $DEFAULT_ZSHRC;
	echo "\x1b[32m$DEFAULT_ZSHRC modded\x1b[0m";
}

echo "\x1b[33mGotta fix brew...\x1b[0m";

## default settings, you can change them if you know what you're doing
DEFAULT_ZSHRC="$HOME/.zshrc"
MODDED_ZSHRC="$HOME/.zshrc.local"
AUTOINST_FILE="$HOME/.brew_autoinstall"

## Allow us to know if a mod is already applied, thanks to uuids.
## $? allow us to get the return value of the previous grep.
## man grep for details
grep -q "d1a5d801-dde1-47e1-8eef-83720894dfd6" $DEFAULT_ZSHRC
if [[ $? -eq 0 ]] ; then
	ZSHRC_MOD_STATUS=1
else
	ZSHRC_MOD_STATUS=0
fi
grep -q "b9b74f04-2c78-458f-895e-45f6db30a898" $MODDED_ZSHRC
if [[ $? -eq 0 ]] ; then
	BREW_MOD_STATUS=1
else
	BREW_MOD_STATUS=0
fi

## Both patch already aplied, ask for confirmation before rm'g the folder
if [[ $ZSHRC_MOD_STATUS -eq 1  && $BREW_MOD_STATUS -eq 1 ]] ; then
	echo "Seems like like the configuration files are already modified";
	echo "\x1b[31mAre you sure you want to reset ~/.brew? All your bottles will be lost [y/N]\x1b[0m \c";
	read yn;
	case $yn in
		## Allow any word beginning by Yy for True
		y*|Y*)
			worker;
		;;
		## Or exit for anything else
		*)
			exit;
		;;
	esac
else
	worker;
fi
